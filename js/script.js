// "use strict"
// ТЕОРІЯ:

/* 
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Щоб передати в рядок "дослівно" (літерально) символ, який є службовим символом.
Приклад 1 — щоб передати символ лапок в строку, яку оголошено точно такими ж лапками, треба перед ним поставити бекслеш.
Приклад 2 — щоб передати в регексп службовий символ "s" як просто букву, треба перед ним поставити бекслеш.
*/



/*
2. Які засоби оголошення функцій ви знаєте?

2.1. function declaration:
function functionName(parameters) { what function does }
(така функція хойстується — підіймається інтерпретатором на початок контексту).

2.2. function expression:
var variableName = function(parameters) { what function does }

2.3. arrow function expression:
(parameters) => { what function does }

2.4. using Function constructor
let add = Function('a', 'b', 'return a+b');

2.5 IFFE () — він існує, але я його не зрозумів.
*/


/*
3. Що таке hoisting, як він працює для змінних та функцій?

Хойстинг ("піднімання" з англійської) — це поведінка інтерпретатора кода, коли він робить вигляд, що функцію чи змінну оголошено на самому початку контексту. Хойстинг дозволяє звертатися до функції чи змінної раніше (вище по тексту кода), ніж її оголошено.
Хойстинг "на повну" працює для функцій, ологошених певним чином, наприклад:
	2.1. function declaration — повністю хойстується,
	2.2. function expression — тут спрацюють правила хойстування змінних (див. нижче): лише декларація може хойстуватися (якщо змінна декларована через var, але не let чи const), але не ініціалізація — тобто, інтерпретатор всередині даного контексту буде бачити var variableName скрізь, але раніше ініціалізації він бачитиме її зі значенням undefined.

Хойстинг частково працює для змінних, оголошених через var (а для змінних, оголошених через let та const, хойстинг зовсім не працює). Що значить "частково": для хойстування змінних, оголошених через var, є обмеження — хойстується лише декларація змінної, але не її ініціалізація: код "console.log(a);var a=1;" видасть в консоль "undefined" — бо на початок контексту підніметься лише декларація (оголошення) змінної "а", але ініціалізація її значенням "1" не передасться на початок контексту. Наступний код видасть в консоль одиницю: "a=1; console.log(a); var a;"

У підсумку, на даний момент мені здається, що користь від хойстингу досить сумнівна. Але далі буде видно :)
*/






// ПРАКТИКА:


function createNewUser() {
	const newUser = {
		firstName: prompt("Введіть імʼя нового юзера:"),
		lastName:  prompt("Введіть прізвище нового юзера:"),
		birthday:  prompt("Введіть дату народження в форматі \"dd.mm.yyyy\""),

		// сеттери для редагування firstName та lastName:
		set setFirstName(val) {
			Object.defineProperty(this, "firstName", { writable: true, });
			this.firstName = val;
			Object.defineProperty(this, "firstName", { writable: false, });
		},

		set setLastName(val) {
			Object.defineProperty(this, "lastName", { writable: true, });
			this.lastName = val;
			Object.defineProperty(this, "lastName", { writable: false, });
		},

		getLogin() {
			return (this.firstName[0] + this.lastName).toLowerCase();
		},

		getPassword() {
			return this.firstName[0].toUpperCase() 
				+  this.lastName.toLocaleLowerCase()
				+  this.birthday.split(".")[2]
			;
		},

		getAge() {
			// Для точності, один рік = 365.24219 днів (згідно Wikipedia).
			// Така точність дозволяє не враховувати вісокосні роки при переводі мілісекунд в роки.
			//
			// Рядок з birthday dd.mm.yyyy розбиваю на складові, 
			// перевертаю масив задом наперед,
			// склеюю через дефіс — так отримуємо рядок у форматі yyyy-mm-dd, який обʼєкт Date спроможний перетворити на дату в мілісекундах,
			// віднімаю отриману дату народження від поточної дати (на момент спрацювання коду),
			// переводжу отриману різницю мілісекунд (вік юзера) в роки,
			// округлюю вниз, щоб отримати кількість повних років.
			return Math.floor((new Date() - new Date(this.birthday.split(".").reverse().join("-")))/1000/3600/24/365.24219);
		},

	};

	// заборона редагувати firstName та lastName напряму, тобто без сеттера:
	Object.defineProperty(newUser, "firstName", { writable: false });
	Object.defineProperty(newUser, "lastName", { writable: false });

	// фінальна перевірка, чи є всі необхідні дані для створення обʼєкта:
	if(newUser.firstName === null 
		|| newUser.lastName === null
		|| newUser.birthday === null) {
		throw new Error("CANNOT CREATE USER: one of parameters is null.");
	} else {
		return newUser;
	};
};

let user1 = createNewUser();

// результат роботи функції:
console.log("результат роботи функції — це обʼєкт \"user1\":\n", user1);

// скільки користувачеві (повних) років:
console.log(user1.getAge());

// пароль користувача:
console.log(user1.getPassword());